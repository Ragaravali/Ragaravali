import { NgModule } from '@angular/core';
import{  ProductlistComponent} from './product/components/productlist/productlist.component';
import{CartlistComponent} from './cart/component/cartlist/cartlist.component';
import {RouterModule, Routes} from '@angular/router';
import{ ProductdetailsComponent} from './product/components/productdetails/productdetails.component';
import{ComponentsComponent} from '../app/login/components/components.component';
const routes :Routes=[
  {
    path:'products',
    component:  ProductlistComponent
  },
  {
    path:'cart',
    component: CartlistComponent
  },
  {
    path:'product-details/:id',
    component:ProductdetailsComponent
  },
  {
    path:'login',
    component:ComponentsComponent
  },
  {
    path:'',
    redirectTo:'/products',
    pathMatch:'full'
    // component:  ProductlistComponent
  }

]
@NgModule({
  imports: [
    
    RouterModule.forRoot(routes)
   
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
