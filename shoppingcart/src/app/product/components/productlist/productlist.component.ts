import { Component, OnInit } from '@angular/core';
import {ProductService } from '../../product.service';
import{ Observable} from 'rxjs';
import Iproduct from '../../../models/product'; 
@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {
  productlist$:Observable<Iproduct[]>;

  constructor(public pdlist:ProductService ) {
 
   }
   det:string;
   click(b)
   {
     this.det=b;
   }

  ngOnInit() {
   this.productlist$= this.pdlist.getproducts();
  }

}
