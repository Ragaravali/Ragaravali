import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { ProductService } from '../../product.service';
import Iproducts from '../../../models/product';
import{Router} from '../../../../../node_modules/@angular/router';
@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.component.html',
  styleUrls: ['./productdetails.component.css']
})
export class ProductdetailsComponent implements OnInit {
  productdetail:Iproducts;
  constructor( private route:ActivatedRoute,
  private service:ProductService,
private router:Router) { }
    viewcart()
    {
   this.router.navigate(['/cart']);

    }
  ngOnInit() {
    const id=this.route.snapshot.paramMap.get("id");
  this. service.getproductbyid(id).subscribe((products:Iproducts)=>{
    this.productdetail=products;
  });
}

}
