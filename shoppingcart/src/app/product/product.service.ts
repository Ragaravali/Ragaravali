import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import Iproducts  from '../models/product';
import {Observable} from 'rxjs';
import{map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
 
  constructor(private http:HttpClient){}
  getproducts():Observable<Iproducts[]>{
    return this.http.get<Iproducts[]>('/assets/data/product.json');
  }
  getproductbyid(id:string):Observable<Iproducts>{
    return this.http.get<Iproducts[]>('/assets/data/product.json')
    .pipe(map((product:Iproducts[])=>{
      return product.find((p:Iproducts)=>p.id===id)
    }))
}
}
