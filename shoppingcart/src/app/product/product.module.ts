import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductlistComponent } from './components/productlist/productlist.component';
import { ProductitemComponent } from './components/productitem/productitem.component';
import { ProductdetailsComponent } from './components/productdetails/productdetails.component';
import{AppRoutingModule} from '../app-routing.module';
import { AddproductComponent } from './components/addproduct/addproduct.component';
@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    ProductlistComponent, ProductitemComponent, ProductdetailsComponent
  ],
  declarations: [ProductlistComponent, ProductitemComponent, ProductdetailsComponent, AddproductComponent]
})
export class ProductModule { }
