import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{FormsModule} from '@angular/forms';
import { ComponentsComponent } from '../login/components/components.component';

@NgModule({
  imports: [
    CommonModule,
    // ComponentsComponent ,
    FormsModule
   
  ],
  declarations: [  ComponentsComponent ]
})
export class LoginModule { }
