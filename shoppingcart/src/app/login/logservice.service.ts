import { Injectable } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import{Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogserviceService {
constructor(private router:Router) { }
public loginchange:Subject<string>=new Subject<string>();
  
private users=[
  {
    email:'email1.com',
    password:"email1"
  },
  {
    email:"email2.com",
    password:"email2"
  },
  {
    email:"email3",
    password:"email3"
  }
]
login(b)
{
 const res=this.users.find((x)=>{return x.email===b.email && x.password===b.password});
 if(res)
 {
   sessionStorage.setItem('useremail',b.email);
   this.router.navigate(['/products']);
   this.loginchange.next('loggedin');
   return true;
 }
 return false;

}
isLoggedIn()
{
  const  useremail=sessionStorage.getItem('useremail');
  return useremail?true:false;
}
logout()
{
  sessionStorage.clear();
}
  
}
