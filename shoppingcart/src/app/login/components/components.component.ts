import { Component, OnInit } from '@angular/core';
import{NgForm} from '@angular/forms';
import{LogserviceService} from '../../login/logservice.service'
@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.css']
})
export class ComponentsComponent implements OnInit {
b;
  constructor(private service:LogserviceService) {
   
   }
  onsubmit(c:NgForm)
  {
  
  this.b= this.service.login(c.value);
  }
  ngOnInit() {
  }

}
