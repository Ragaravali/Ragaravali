import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinicartComponent } from './component/minicart/minicart.component';
import { CartlistComponent } from './component/cartlist/cartlist.component';
import { CheckoutComponent } from './component/checkout/checkout.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    MinicartComponent, CartlistComponent, CheckoutComponent
  ],
  declarations: [MinicartComponent, CartlistComponent, CheckoutComponent]
})

export class CartModule { }
