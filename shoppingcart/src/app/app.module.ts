import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import{ ProductModule } from './product/product.module';
import{ CartModule } from './cart/cart.module';
import{AppRoutingModule} from '../app/app-routing.module';
import{ LoginModule} from'../app/login/login.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ProductModule ,
    CartModule ,
    HttpClientModule,
    AppRoutingModule,
    LoginModule
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
