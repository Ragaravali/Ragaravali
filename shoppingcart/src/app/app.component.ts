import { Component } from '@angular/core';
import{AppserviceService} from './appservice.service';
import{LogserviceService} from './login/logservice.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  selectnow:string;
  isloggedin:any
  onclick(a)
  {
    this.selectnow=a;
  }
  logout(){
  sessionStorage.getItem('useremail');
 }
  constructor(private service:AppserviceService,
  private logoutserv:LogserviceService){

  }
  onlogout()
  {
    this.logoutserv.logout();
    this.isloggedin="";
  }
  ngOnInit(){
    this.service.viewchange.subscribe(viewname=>{
      this.selectnow=viewname;
    });
    this.isloggedin=this.logoutserv.isLoggedIn();
    this.logoutserv.loginchange.subscribe(isLoggedIn=>{
      this.isloggedin=isLoggedIn;
    });
  }
 
}
