import { Injectable } from '@angular/core';
import{Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppserviceService {
  public viewchange:Subject<string>=new Subject<string>();
  constructor() { }
}
